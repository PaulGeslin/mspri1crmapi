use actix_web::{App, HttpServer};
use paperclip::actix::OpenApiExt;
use std::{env, io};

#[path = "Models/Customer.rs"]
mod Customer;
//https://gitlab.com/hook0/hook0
#[actix_web::main]
async fn main() -> io::Result<()> {
    env::set_var("RUST_LOG", "actix_web=debug,actix_server=info");

    HttpServer::new(|| {
        {
            App::new()
                // .wrap_api()
                // .with_json_spec_at("/api/spec")
                // register HTTP requests handlers
                .service(Customer::list)
                //.service(Customer::list_by_id)
        }
    })
    .bind("127.0.0.1:9090")?
    .run()
    .await
}
