use actix_web::get;
use paperclip::actix::{
    api_v2_operation,
    web::{Data, Json, Path, Query},
    Apiv2Schema, CreatedJson, NoContent,
};
use serde::{Serialize, Deserialize};

//#[api_v2_operation]
#[get("/customers")]
pub async fn list() -> Result<String, Box<dyn std::error::Error>> {
    let client = reqwest::Client::new();
    let body = client
        .get("https://615f5fb4f7254d0017068109.mockapi.io/api/v1/customers/")
        .send()
        .await?
        .text()
        .await?;

    Ok(body)
}
// #[api_v2_operation(
//     summary = "Customer by id",
//     description = "",
//     operation_id = "Customer.list_by_id",
//     consumes = "application/json",
//     produces = "application/json",
//     tags("Applications Management")
// )]
// #[get("/customers/{id}")]
// pub async fn list_by_id(id: String) -> Result<String, Box<dyn std::error::Error>> {

//     println!("{}", id);

//     let client = reqwest::Client::new();

//     let mut url = "https://615f5fb4f7254d0017068109.mockapi.io/api/v1/customers/".to_string();

//     url.push_str(&id);
   
//     let body = client.get(url).send().await?.text().await?;

//     Ok(body)
// }}

//